# https://hub.docker.com/_/python
FROM python:3.7.10

#WORKDIR /app

# Install dependencies
COPY requirements-dp.txt /
RUN pip install -r requirements-dp.txt

# Copy code
COPY src /src

# Copy saved models
COPY saved_models/full_ann_dot_no_finetune.db /saved_models

# Load DeepPavlov model
RUN python -m deeppavlov install squad_ru_rubert_infer

EXPOSE 8000

# cmd for running the API
CMD ["gunicorn", "src.rest_api.app:app",  "--bind", "0.0.0.0:8000", "-k", "uvicorn.workers.UvicornWorker", "--limit-concurrency", "3", "--timeout", "60"]
