from src.logger import get_logger
from typing import List

import numpy as np
import torch
from farm.data_handler.dataloader import NamedDataLoader
from farm.data_handler.processor import TextSimilarityProcessor
from farm.modeling.biadaptive_model import BiAdaptiveModel
from farm.modeling.language_model import LanguageModel
from farm.modeling.prediction_head import TextSimilarityHead
from farm.modeling.tokenization import Tokenizer
from torch.utils.data import SequentialSampler
from tqdm import tqdm

from src.retriever.base_retriever import BaseRetriever

logger = get_logger(__name__)


class DensePassageRetriever(BaseRetriever):
    """
    https://github.com/deepset-ai/haystack/blob/master/haystack/retriever/dense.py
    """

    def __init__(
        self,
        query_model_path: str,
        passage_model_path: str,
        batch_size: int = 16,
        max_seq_len_query: int = 64,
        max_seq_len_passage: int = 256,
        use_gpu: bool = True,
    ):
        self.batch_size = batch_size
        self.max_seq_len_query = max_seq_len_query
        self.max_seq_len_passage = max_seq_len_passage
        self.device = torch.device('cuda') if use_gpu else torch.device('cpu')

        # Init & Load Encoders
        query_tokenizer = Tokenizer.load(
            pretrained_model_name_or_path=query_model_path,
            do_lower_case=True,
            tokenizer_class="DPRQuestionEncoderTokenizer")
        query_encoder = LanguageModel.load(
            pretrained_model_name_or_path=query_model_path,
            language_model_class="DPRQuestionEncoder")
        passage_tokenizer = Tokenizer.load(
            pretrained_model_name_or_path=passage_model_path,
            do_lower_case=True,
            tokenizer_class="DPRContextEncoderTokenizer")
        passage_encoder = LanguageModel.load(
            pretrained_model_name_or_path=passage_model_path,
            language_model_class="DPRContextEncoder")

        self.processor = TextSimilarityProcessor(
            query_tokenizer=query_tokenizer,
            passage_tokenizer=passage_tokenizer,
            max_seq_len_passage=max_seq_len_passage,
            max_seq_len_query=max_seq_len_query,
            label_list=["hard_negative", "positive"],
            metric="text_similarity_metric",
            embed_title=True,
            num_hard_negatives=0,
            # num_positives=1,
        )

        prediction_head = TextSimilarityHead(similarity_function="dot_product")

        self.model = BiAdaptiveModel(
            language_model1=query_encoder,
            language_model2=passage_encoder,
            prediction_heads=[prediction_head],
            embeds_dropout_prob=0.1,
            lm1_output_types=["per_sequence"],
            lm2_output_types=["per_sequence"],
            device=self.device,
        )
        self.model.connect_heads_with_processor(self.processor.tasks, require_labels=False)

    def transform(self, dicts: List[dict]):
        logger.info("Creating dataset from dicts")

        dataset, tensor_names, _ = self.processor.dataset_from_dicts(
            dicts, indices=[i for i in range(len(dicts))],
        )

        logger.info("Creating DataLoader")
        data_loader = NamedDataLoader(
            dataset=dataset, sampler=SequentialSampler(dataset),
            batch_size=self.batch_size, tensor_names=tensor_names,
        )

        all_query_embeddings = list()
        all_passage_embeddings = list()

        self.model.eval()

        with tqdm(
            total=len(dicts), unit="batches",
            desc=f"Creating embeddings for batch",
            position=0, leave=True,
        ) as progress_bar:
            for batch in data_loader:
                batch = {key: batch[key].to(self.device) for key in batch}

                with torch.no_grad():
                    query_embedding, passage_embedding = self.model.forward_lm(**batch)
                    if query_embedding is not None:
                        all_query_embeddings.append(query_embedding.cpu().numpy())
                    if passage_embedding is not None:
                        all_passage_embeddings.append(passage_embedding.cpu().numpy())

                progress_bar.update(self.batch_size)
            progress_bar.close()

        if all_query_embeddings:
            all_query_embeddings = np.concatenate(all_query_embeddings)
            return all_query_embeddings
        if all_passage_embeddings:
            all_passage_embeddings = np.concatenate(all_passage_embeddings)
            return all_passage_embeddings
        # return all_query_embeddings, all_passage_embeddings

    def retrieve_embedding(self, query: str) -> np.array:
        queries = [query]

        queries = [{'query': q} for q in queries]
        query_embedding = self.transform(queries)

        return query_embedding

    def retrieve_documents(self, documents):
        passages = [{
            'passages': [{
                "title": "",
                "text": d.text,
                "label": "positive",
                "external_id": d.id
            }]
        } for d in documents]

        embeddings = self.transform(passages)

        return embeddings
