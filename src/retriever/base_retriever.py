from abc import abstractmethod
from typing import List

import numpy as np


class BaseRetriever:
    """
    Базовый класс для создания эмбеддингов документов.
    """

    @abstractmethod
    def transform(self, dicts: List[dict]) -> np.array:
        """
        Метод для преобразования списка документа в эмбеддинги.
        """
        pass
