from abc import abstractmethod

import numpy as np

from src.document_store.sql_store import SQLDocumentStore
from src.retriever.base_retriever import BaseRetriever


class BaseIndexer:
    """
    Базовый класс индекса для поиска документов.
    """
    index: str

    @abstractmethod
    def build(self, store: SQLDocumentStore, retriever: BaseRetriever, batch_size: int) -> None:
        """
        Метод для создания поискового индекса.
        """
        pass

    @abstractmethod
    def search(
        self,
        query_embedding: np.array,
        limit: int,
    ):
        """
        Метод для поиска релевантных документов для ответов на вопрос.
        """
        pass

    @abstractmethod
    def save(self, name: str) -> None:
        """
        Метод для сохранения созданного индекса в файл
        """
        pass

    @abstractmethod
    def load(self) -> None:
        """
        Метод загрузки файла индекса
        """
        pass
