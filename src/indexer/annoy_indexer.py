from pathlib import Path
from typing import Union

import numpy as np
from annoy import AnnoyIndex
from tqdm import tqdm

from src.document_store.sql_store import SQLDocumentStore
from src.indexer.base_indexer import BaseIndexer
from src.logger import get_logger
from src.retriever.dpr_retriever import DensePassageRetriever
from src.utils import get_batches_from_generator

logger = get_logger(__name__)


class AnnoyIndexer(BaseIndexer):
    """
    Builds an search model using the Annoy. See https://github.com/spotify/annoy
    """
    def __init__(
        self,
        index,
        model_path: Union[str, Path],
        metric="angular",
        n_trees=10,
        embedding_size=768,
    ):
        self.progress_bar = True
        self.annoy_index = None
        self.metric = metric
        self.n_trees = n_trees
        self.embedding_size = embedding_size
        self.index = index
        self.model_path = model_path

    def build(
        self,
        store: SQLDocumentStore,
        retriever: DensePassageRetriever,
        batch_size: int = 10000,
    ):
        self.annoy_index = AnnoyIndex(self.embedding_size, self.metric)
        self.annoy_index.set_seed(42)
        vector_id = self.annoy_index.get_n_items()

        document_count = store.get_document_count()

        logger.info(f"Loading documents from database...")
        doc_generator = store.get_all_documents_generator()
        batched_documents = get_batches_from_generator(doc_generator, batch_size)
        logger.info(f"Documents loaded...")

        with tqdm(total=document_count, desc="Creating embeddings") as progress_bar:
            for document_batch in batched_documents:
                embeddings = retriever.retrieve_documents(document_batch)

                logger.info(f"Writing vectors for batch...")
                for i, doc in enumerate(document_batch):
                    store.write_vector(vector_id, self.index, doc.id)

                    self.annoy_index.add_item(vector_id, embeddings[i])
                    vector_id += 1

                store.session_commit()
                progress_bar.update(batch_size)
            progress_bar.close()

        self.annoy_index.build(self.n_trees)
        self.save(self.model_path)

    # TODO: add update_docs method
    def update_documents(self, documents):
        pass

    def search(
        self,
        query_embedding: np.array,
        limit: int = 10,
    ):
        vector_ids, scores = self.annoy_index.get_nns_by_vector(
            query_embedding.reshape((-1,)), n=limit, search_k=-1, include_distances=True)
        return vector_ids, scores

    def save(self, name: str):
        self.annoy_index.save(name)

    def load(self):
        self.annoy_index = AnnoyIndex(self.embedding_size, self.metric)
        self.annoy_index.load(self.model_path)
