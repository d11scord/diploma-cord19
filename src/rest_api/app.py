import uvicorn
from fastapi.responses import HTMLResponse
from fastapi.requests import Request

from src.document_store.schema import DocumentORM
from src.rest_api.setup_app import create_app, generate_examples, prepare_answers

app, templates, pipeline, ru_pipe_test = create_app()


@app.get("/", response_class=HTMLResponse)
def root(request: Request):
    examples = generate_examples()
    return templates.TemplateResponse("main_page.html", {"request": request, "examples": examples})


@app.get("/search", response_class=HTMLResponse)
def search_documents(request: Request, question: str):
    answers = pipeline.process_question(question)
    answers = prepare_answers(answers)
    """
    {
        'start_ctx': 'ry droplets during close contact. ', 
        'answer': 'It is most common in school-aged children, military recruits, and college students', 
        'end_ctx': '.[11] Most cases occur singly or a', 
        'score': 7.686141490936279, 
        'probability': 0.7232754823942319, 
        'context': 'ry droplets during close contact. It is most common in ...', 
        'offset_start_in_doc': 8432, 
        'offset_end_in_doc': 8514, 
        'document_id': 1, 
        'doc': Document(
            id=1, 
            title='Clinical features of culture-proven Mycoplasma pneumoniae infections at ...', 
            text='Mycoplasma pneumoniae is a common cause of ...', 
            cord_uid='ug7v899j', 
            sha='d1aafb70c066a2068b02786f8929fd9c900897fb', 
            source='PMC', 
            doi='10.1186/1471-2334-1-6', 
            pmcid='PMC35282', 
            license='no-cc', 
            publish_time=datetime.date(2001, 7, 4), 
            authors='Madani, Tariq A; Al-Ghamdi, Aisha A', 
            journal='BMC Infect Dis', 
            url='https://www.ncbi.nlm.nih.gov/pmc/articles/PMC35282/'
        )
    }
    """
    return templates.TemplateResponse("results.html", {"request": request, "answers": answers, "question": question})


@app.get("/document/{doc_id}")
def get_document_by_id(
    request: Request,
    doc_id: int,
):
    document: DocumentORM = pipeline.get_document_by_id(doc_id)
    if document:
        return templates.TemplateResponse("document_page.html", {
            "request": request,
            "doc_title": document.title,
            "doc_text": document.text,
            "meta": document.meta,
        })


@app.get("/ru_qa", response_class=HTMLResponse)
def ru_qa_example(request: Request):
    questions, answers = ru_pipe_test.process_questions()
    qas = {q: a for q, a in zip(questions, answers)}
    return templates.TemplateResponse("test_results.html", {"request": request, "qas": qas})


if __name__ == "__main__":
    uvicorn.run(app, host="127.0.0.1", port=8000, log_level="info")
