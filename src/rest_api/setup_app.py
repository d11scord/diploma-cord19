import random
from pathlib import Path

from fastapi import FastAPI
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates

from src.document_store.schema import DatabaseSession
from src.document_store.sql_store import SQLDocumentStore
from src.indexer.annoy_indexer import AnnoyIndexer
from src.logger import setup_applevel_logger
from src.pipeline.deeppavlov_pipeline import DeepPavlovPipeline
from src.pipeline.farm_pipeline import FARMPipeline
from src.reader.deeppavlov_reader import DeepPavlovReader
from src.reader.farm_reader import FARMReader
from src.retriever.dpr_retriever import DensePassageRetriever


logger = setup_applevel_logger()


# TODO: add env variables
def setup_pipeline():
    index = "annoy_angular_500trees"
    use_gpu = False

    current_file = Path(__file__)
    current_file_dir = current_file.parent
    project_root_path = current_file_dir.parent.parent
    saved_models_path = project_root_path / "saved_models"

    session = DatabaseSession(
        url=f"sqlite:///{saved_models_path}/full_2021-05-17.db"
    ).get_session()
    store = SQLDocumentStore(session)

    query_embedding_model = "facebook/dpr-question_encoder-single-nq-base"
    passage_embedding_model = "facebook/dpr-ctx_encoder-single-nq-base"

    retriever = DensePassageRetriever(
        str(query_embedding_model),
        str(passage_embedding_model),
        use_gpu=use_gpu,
    )
    indexer = AnnoyIndexer(index=index, metric="angular", n_trees=500,
                           model_path=str(saved_models_path / f"{index}.ann"))
    indexer.load()
    reader = FARMReader(
        model_name_or_path="deepset/roberta-base-squad2",
        use_gpu=use_gpu,
    )

    pipe = FARMPipeline(
        index=index,
        session=session,
        store=store,
        retriever=retriever,
        indexer=indexer,
        reader=reader,
    )

    ru_reader = DeepPavlovReader("squad_ru_rubert_infer")
    ru_pipe = DeepPavlovPipeline(ru_reader)
    return pipe, ru_pipe


def create_app():
    app = FastAPI()

    current_file = Path(__file__)
    current_file_dir = current_file.parent
    app.mount("/static", StaticFiles(directory=str(current_file_dir / "static")), name="static")
    templates = Jinja2Templates(directory=str(current_file_dir / "templates"))
    pipe, ru_pipe_test = setup_pipeline()

    return app, templates, pipe, ru_pipe_test


def generate_examples(num_examples: int = 3):
    examples = [
        "What is the main cause of HIV-1 infection in children?",
        "How does being a smoker impact COVID-19 patient outcomes?",
        "Where was COVID19 first discovered?",
        "Where can published genomic sequences be found for the 2019-nCoV virus?",
        "How is 2019-nCOV transmitted?",
        "In 2009 what was the reported H1N1 vaccination rate in China?",
        "What's the recommended procedure to disinfect at CT scanner after a COVID-19 exposure?",
        "What animals can carry coronavirus?",
        "Which mask is recommended for healthcare workers?",  # https://www.covid19treatmentguidelines.nih.gov/critical-care/
        "Is Remdesivir recommended for covid treatment?",     # https://www.covid19treatmentguidelines.nih.gov/therapeutic-management/
    ]
    e = random.sample(examples, num_examples)
    return e


def prepare_answers(answers):
    result = list()
    for answer in answers:
        start_idx = answer.pop("offset_start")
        end_idx = answer.pop("offset_end")
        probability = answer.pop("probability")
        result.append({
            "start_ctx": answer["context"][:start_idx],
            "answer": answer["answer"].strip(),
            "end_ctx": answer["context"][end_idx:],
            "probability": round(probability, 3),
            **answer,
        })

    return result
