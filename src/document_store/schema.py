from dataclasses import dataclass

from sqlalchemy import Column, String, Text, Integer, ForeignKey, Date
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, relationship, scoped_session

Base = declarative_base()


class DocumentORM(Base):
    __tablename__ = "document"

    id = Column(Integer, primary_key=True)
    title = Column(String, nullable=False)
    text = Column(Text, nullable=False)

    meta = relationship("MetadataORM", uselist=False, back_populates="document", lazy="joined")


class VectorORM(Base):
    __tablename__ = "vectors"

    id = Column(Integer, primary_key=True)
    vector_id = Column(Integer, nullable=False)
    index = Column(String(100), nullable=False)
    document_id = Column(
        Integer,
        ForeignKey("document.id", ondelete="CASCADE", onupdate="CASCADE"),
        nullable=False,
        index=True,
    )
    documents = relationship(DocumentORM, backref="vectors")


class MetadataORM(Base):
    __tablename__ = "metadata"

    id = Column(Integer, primary_key=True)
    cord_uid = Column(String(20), nullable=False)  # TODO: add unique=True
    sha = Column(String(40))
    source = Column(String(50), nullable=False)
    doi = Column(String(40))
    pmcid = Column(String(40))
    license = Column(String, nullable=False)
    publish_time = Column(Date)
    authors = Column(String(150))
    journal = Column(String(150))
    url = Column(String(200), nullable=False)
    document_id = Column(
        Integer,
        ForeignKey("document.id", ondelete="CASCADE", onupdate="CASCADE"),
        nullable=False,
        index=True,
    )
    document = relationship(DocumentORM, back_populates="meta")


class DatabaseSession:
    def __init__(
        self,
        url: str = "sqlite:///file.db",  # in memory
    ):
        """
        Хранилище документов в реляционной БД.

        :param url: Ссылка на базу данных для SQLAlchemy. Подробнее на:
                    https://docs.sqlalchemy.org/en/13/core/engines.html#database-urls
        """
        self.url = url
        engine = create_engine(self.url)
        session_factory = sessionmaker(bind=engine)
        # Треды ломают БД, поэтому используем scoped_session
        # https://docs.sqlalchemy.org/en/13/orm/contextual.html#using-thread-local-scope-with-web-applications
        # https://copdips.com/2019/05/using-python-sqlalchemy-session-in-multithreading.html
        self.session = scoped_session(session_factory)
        Base.metadata.create_all(engine)

    def get_session(self):
        return self.session


@dataclass
class Document:
    id: int
    title: str
    text: str
    cord_uid: str
    sha: str
    source: str
    doi: str
    pmcid: str
    license: str
    publish_time: Date
    authors: str
    journal: str
    url: str
