from datetime import date
from typing import List

from tqdm import tqdm

from src.document_store.schema import DocumentORM, VectorORM, MetadataORM, Document
from src.logger import get_logger
from src.utils import get_batches_from_generator, get_batches_from_list


logger = get_logger(__name__)


class SQLDocumentStore:
    def __init__(self, session):
        self.session = session

    def get_document_by_id(self, doc_id: int):
        return self.session.query(DocumentORM).get(doc_id)

    def get_all_documents_generator(self, batch_size=5000):
        documents_query = self.session.query(
            DocumentORM.id,
            DocumentORM.text,
        )

        documents_map = dict()
        for i, doc in enumerate(documents_query, start=1):
            # TODO: add class Document
            documents_map[doc.id] = doc

            if i % batch_size == 0:
                yield from documents_map.values()
                documents_map = {}
        if documents_map:
            yield from documents_map.values()

    def write_documents(self, dicts, batch_size: int = 5000) -> None:
        if isinstance(dicts, list):
            batched_documents = get_batches_from_list(dicts, batch_size)
        else:
            batched_documents = get_batches_from_generator(dicts, batch_size)

        logger.info("Writing documents to database...")
        for document_batch in tqdm(
            batched_documents, desc="Writing documents to database",
            unit="Doc",
        ):
            for d in document_batch:
                publish_time = [int(d_) for d_ in d["publish_time"].split("-")]
                try:
                    pt = date(*publish_time)
                except TypeError:
                    pt = date(*publish_time, 1, 1)
                meta = MetadataORM(
                    cord_uid=d["cord_uid"],
                    sha=d["sha"],
                    source=d["source"],
                    doi=d["doi"],
                    pmcid=d["pmcid"],
                    license=d["license"],
                    publish_time=pt,
                    authors=d["authors"],
                    journal=d["journal"],
                    url=d["url"],
                )
                new_doc = DocumentORM(
                    title=d["title"],
                    text=d["text"],
                    meta=meta,
                )
                self.session.add(new_doc)

            self.session_commit()

    def get_documents_by_vector_ids(
        self,
        vector_ids: List[int],
        index: str,
    ) -> List[Document]:
        documents_orm = (
            self.session.query(DocumentORM)
            .join(VectorORM, DocumentORM.id == VectorORM.document_id)
            .filter(VectorORM.index == index)
            .filter(VectorORM.vector_id.in_(vector_ids))
        ).all()

        documents = list()
        for doc in documents_orm:
            new_doc = Document(
                id=doc.id,
                title=doc.title,
                text=doc.text,
                cord_uid=doc.meta.cord_uid,
                sha=doc.meta.sha,
                source=doc.meta.source,
                doi=doc.meta.doi,
                pmcid=doc.meta.pmcid,
                license=doc.meta.license,
                publish_time=doc.meta.publish_time,
                authors=doc.meta.authors,
                journal=doc.meta.journal,
                url=doc.meta.url,
            )
            documents.append(new_doc)

        return documents

    def get_document_count(self) -> int:
        count = self.session.query(DocumentORM).count()

        return count

    def write_vector(self, vector_id: int, index: str, document_id: int):
        vec = VectorORM(
            vector_id=vector_id,
            index=index,
            document_id=document_id,
        )
        self.session.add(vec)

    def session_commit(self):
        try:
            self.session.commit()
        except Exception as ex:
            logger.error(f"Transaction rollback: {ex.__cause__}")
            self.session.rollback()
            raise ex
