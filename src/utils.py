import csv
import json
import os
from itertools import islice
from typing import List


def covidqa_to_dict(covidqa_filepath: str) -> List[dict]:
    """
    Скрипт для открытия датасета COVID-QA.json. Структура файла:

    {
        "data": [
            "paragraphs": [
                {
                    "context": "Functional Genetic Variants in DC-SIGNR Are Associated with...",
                    "document_id": 630,
                    "qas": [
                        {
                            "answers": [{
                                "answer_start": 370,
                                "text": "Mother-to-child transmission (MTCT) is the main cause..."
                            }],
                            "id": 262,
                            "is_impossible": False,
                            "question": "What is the main cause of HIV-1 infection in children?"
                        },
                        {
                            "answers": [{
                                "answer_start": 2003,
                                "text": "DC-SIGNR plays a crucial role in..."
                            }],
                            "id": 276,
                            "is_impossible": False,
                            "question": "What plays the crucial role in..."
                        },
                        ...
                    ]
                }
            ],
            "paragraphs": [
                {
                    "context": "Role of S-Palmitoylation on IFITM5 for the Interaction with...",
                    "document_id": 650,
                    "qas": [{...}, {...}, ...],
                },
            ],
            ...
        ]
    }

    :param covidqa_filepath: Путь к файлу COVID-QA.json
    :return: Данные для заполнения таблицы Passages,
             которые будут поданы на вход для Dense Passage Retriever.

             Формат данных:
             [
                 {
                     "passages": [
                         {
                            "title": "",
                            "text": "Functional Genetic Variants in DC-SIGNR Are Associated with...",
                            "label" "positive",
                            "external_id": 630,
                         }
                    ]
                 },
                 {
                     "passages": [
                        {...}
                    ]
                 },
                 ...
            ]

    """
    with open(covidqa_filepath, "r", encoding="utf-8") as f:
        covidqa = json.load(f)

    print("Length of COVID-QA dataset:", len(covidqa["data"]))

    dicts = [{
        "passages": [{
            "title": "",
            "text": d["paragraphs"][0]["context"],
            "label": "positive",
            "external_id": d["paragraphs"][0]["document_id"],
        }]
    } for d in covidqa["data"]]

    return dicts


def covidqa_to_dicts(covidqa_filepath: str):
    with open(covidqa_filepath, "r", encoding="utf-8") as f:
        covidqa = json.load(f)

    dicts = [{
        "document_id": d["paragraphs"][0]["document_id"],
        "text": d["paragraphs"][0]["context"],
    } for d in covidqa["data"]]

    return dicts


def open_json_document(filepath: str):
    try:
        with open(filepath) as json_file:
            document_json = json.load(json_file)
        return document_json
    except IsADirectoryError:
        return None


def load_metadata(metadata_path: str, pmc_json_path: str, pdf_json_path: str, limit):
    with open(metadata_path, encoding="utf-8") as f:
        reader = csv.reader(f)

        columns = next(reader)
        print("Columns:", ", ".join(columns))

        total_skipped = 0

        for i, row in enumerate(reader):
            if limit and i == limit:
                break
            # TODO: сделать покрасивее
            cord_uid, sha, source, title, \
            doi, pmcid, pubmed_id, license, abstract, \
            publish_time, authors, journal, _, \
            who_covidence_id, arxiv_id, \
            pdf_json_file, pmc_json_file, url, s2_id = row

            if not pmc_json_file and not pdf_json_file:
                total_skipped += 1
                print(f"Empty doc {i}. Skip that document... Total skipped: {total_skipped}")
                continue
            if pmc_json_file:
                document_json = open_json_document(pmc_json_path + pmc_json_file.split("/")[-1])
            elif pdf_json_file:
                document_json = open_json_document(pdf_json_path + pdf_json_file.split("/")[-1])

            document_abstract = []
            if "abstract" in document_json["metadata"].keys():
                for j in document_json["metadata"]["abstract"]:
                    document_abstract.append(j["text"])

            document_text = [t["text"] for t in document_json["body_text"]]
            document = "\n\n".join(document_abstract + document_text)
            yield {
                "title": title,
                "authors": authors,
                "cord_uid": cord_uid,
                "sha": sha,
                "source": source,
                "doi": doi,
                "pmcid": pmcid,
                "license": license,
                "publish_time": publish_time,
                "journal": journal,
                "pdf_json_file": pdf_json_file,
                "pmc_json_file": pmc_json_file,
                "url": url,
                "text": document,
            }


def load_json_files(pmc_json_path: str, limit):
    files = os.listdir(pmc_json_path)
    print("Количество файлов в папке", pmc_json_path, ":", len(files))

    for i, f in enumerate(files):
        if i == limit:
            break

        with open(pmc_json_path + f) as json_file:
            document_json = json.load(json_file)

        document_abstract = []
        if "abstract" in document_json["metadata"].keys():
            for j in document_json["metadata"]["abstract"]:
                document_abstract.append(j["text"])

        document_text = [t["text"] for t in document_json["body_text"]]
        document = "\n".join(document_abstract + document_text)
        yield {"text": document}


def get_batches_from_generator(iterable, n):
    """
    Batch elements of an iterable into fixed-length chunks or blocks.
    """
    it = iter(iterable)
    x = tuple(islice(it, n))
    while x:
        yield x
        x = tuple(islice(it, n))


def get_batches_from_list(lst, n):
    """
    Yield successive n-sized chunks from list.
    """
    for i in range(0, len(lst), n):
        yield lst[i:i + n]
