python train.py \
    --pretrained_model_name_or_path deepset/roberta-base-squad2 \
    --data_dir ../data \
    --train_filename COVID-QA.json \
    --save_dir ../saved_models \
    --random_seed 42