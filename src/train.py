import argparse
import logging

from farm.utils import set_all_seeds, MLFlowLogger, initialize_device_settings
from farm.modeling.tokenization import Tokenizer
from farm.data_handler.processor import SquadProcessor
from farm.data_handler.data_silo import DataSilo
from farm.modeling.adaptive_model import AdaptiveModel
from farm.modeling.optimization import initialize_optimizer
from farm.train import Trainer


logger = logging.getLogger(__name__)


def main():
    parser = argparse.ArgumentParser()

    # Required parameters
    parser.add_argument("--pretrained_model_name_or_path", default=None, type=str, required=True)
    parser.add_argument("--data_dir", type=str, required=True)
    parser.add_argument("--train_filename", type=str, required=True)

    parser.add_argument("--save_dir", default=None, type=str, required=True)

    # Other parameters
    parser.add_argument("--batch_size", type=int, default=1)
    parser.add_argument("--n_epochs", type=int, default=2)
    parser.add_argument("--learning_rate", type=float, default=1e-5)

    parser.add_argument("--use_gpu", type=bool, default=True)
    parser.add_argument("--use_amp", type=bool, default=False)
    parser.add_argument("--random_seed", type=int, default=42)

    parser.add_argument("--dev_split", type=int, default=0)
    parser.add_argument("--dev_filename", type=str, default=None)
    parser.add_argument("--test_filename", type=str, default=None)

    parser.add_argument("--max_seq_len", type=int, default=256)
    parser.add_argument("--doc_stride", type=int, default=128)
    parser.add_argument("--do_lower_case", type=bool, default=False)
    parser.add_argument("--evaluate_every", type=int, default=0)
    parser.add_argument("--no_ans_boost", type=float, default=0.0)
    parser.add_argument("--accuracy_at", type=int, default=3)
    parser.add_argument("--context_window_size", type=int, default=150)
    parser.add_argument("--top_k_per_sample", type=int, default=1)

    parser.add_argument("--schedule_name", type=str, default="LinearWarmup")
    parser.add_argument("--num_warmup_steps", type=float, default=0.2)

    args = parser.parse_args()

    # Логгирование
    ml_logger = MLFlowLogger(tracking_uri="ml_logs")
    ml_logger.init_experiment(
        experiment_name=args.pretrained_model_name_or_path,
        run_name=args.pretrained_model_name_or_path,
    )

    set_all_seeds(seed=args.random_seed)
    device, n_gpu = initialize_device_settings(use_cuda=True)

    # 1.Create a tokenizer
    tokenizer = Tokenizer.load(
        pretrained_model_name_or_path=args.pretrained_model_name_or_path,
        do_lower_case=args.do_lower_case)

    # 2. Create a DataProcessor that handles all the conversion from raw text into a pytorch Dataset
    processor = SquadProcessor(
        tokenizer=tokenizer,
        max_seq_len=args.max_seq_len,
        label_list=["start_token", "end_token"],
        metric="squad",
        train_filename=args.train_filename,
        dev_filename=args.dev_filename,
        dev_split=args.dev_split,
        test_filename=args.test_filename,
        data_dir=args.data_dir,
        doc_stride=args.doc_stride,
    )

    # 3. Create a DataSilo that loads several datasets (train/dev/test), provides DataLoaders for them and calculates
    # a few descriptive statistics of our datasets
    data_silo = DataSilo(
        processor=processor,
        batch_size=args.batch_size)

    # fine-tune pre-trained question-answering model
    model = AdaptiveModel.convert_from_transformers(
        args.pretrained_model_name_or_path, device=device, task_type="question_answering")
    model.connect_heads_with_processor(data_silo.processor.tasks, require_labels=True)
    # If positive, thjs will boost "No Answer" as prediction.
    # If negative, this will prevent the model from giving "No Answer" as prediction.
    model.prediction_heads[0].no_ans_boost = args.no_ans_boost
    # Number of predictions the model will make per Question.
    # The multiple predictions are used for evaluating top n recall.
    model.prediction_heads[0].n_best = args.accuracy_at
    model.prediction_heads[0].context_window_size = args.context_window_size
    model.prediction_heads[0].n_best_per_sample = args.top_k_per_sample

    # Create an optimizer
    model, optimizer, lr_schedule = initialize_optimizer(
        model=model,
        learning_rate=args.learning_rate,
        device=device,
        n_batches=len(data_silo.loaders["train"]),
        n_epochs=args.n_epochs,
        use_amp=args.use_amp,
        schedule_opts={
            "name": args.schedule_name,
            "num_warmup_steps": args.num_warmup_steps,
        }
    )

    # 4. Feed everything to the Trainer, which keeps care of growing our model and evaluates it from time to time
    trainer = Trainer(
        model=model,
        optimizer=optimizer,
        data_silo=data_silo,
        epochs=args.n_epochs,
        n_gpu=n_gpu,
        lr_schedule=lr_schedule,
        evaluate_every=args.evaluate_every,
        device=device,
        evaluator_test=False,
        eval_report=False
    )

    # train it
    trainer.train()

    model.save(args.save_dir)
    processor.save(args.save_dir)


if __name__ == "__main__":
    main()
