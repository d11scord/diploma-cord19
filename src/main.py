from pprint import pprint

from src.document_store.sql_store import SQLDocumentStore
from src.indexer.annoy_indexer import AnnoyIndexer
from src.pipeline.farm_pipeline import FARMPipeline
from src.reader.farm_reader import FARMReader

from src.retriever.dpr_retriever import DensePassageRetriever
from src.document_store.schema import DatabaseSession
from src.utils import load_metadata

index = "annoy_angular_500trees"
use_gpu = True

current_file = Path(__file__)
current_file_dir = current_file.parent
project_root_path = current_file_dir.parent.parent
saved_models_path = project_root_path / "saved_models"

session = DatabaseSession(
    url=f"sqlite:///{saved_models_path}/full_2021-05-17.db"
).get_session()

dicts = load_metadata(
    metadata_path="../../metadata.csv",
    pmc_json_path=r"../../pmc_json/",
    pdf_json_path=r"../../pdf_json/",
    limit=0,
)

store = SQLDocumentStore(session)
store.write_documents(dicts)
print("Total documents", store.get_document_count())

retriever = DensePassageRetriever(
    "facebook/dpr-question_encoder-single-nq-base",
    "facebook/dpr-ctx_encoder-single-nq-base",
    use_gpu=use_gpu,
)

indexer = AnnoyIndexer(index=index, metric="angular", n_trees=500, model_path=f"{saved_models_path}/{index}.ann")
indexer.build(store, retriever)
indexer.load()

reader = FARMReader(model_name_or_path="deepset/roberta-base-squad2", use_gpu=use_gpu)

pipe = FARMPipeline(
    index=index,
    session=session,
    store=store,
    retriever=retriever,
    indexer=indexer,
    reader=reader,
)

# question = "How effective is Dexamethasone?"
question = "Is Remdesivir recommended for covid treatment?"

result = pipe.process_question(question)

pprint(result)
