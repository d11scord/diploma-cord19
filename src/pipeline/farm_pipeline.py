import logging

from src.document_store.schema import DatabaseSession
from src.document_store.sql_store import SQLDocumentStore
from src.indexer.base_indexer import BaseIndexer
from src.reader.farm_reader import FARMReader
from src.retriever.dpr_retriever import DensePassageRetriever

logger = logging.getLogger(__name__)


class FARMPipeline:
    def __init__(
        self,
        index: str,
        session: DatabaseSession,
        store: SQLDocumentStore,
        retriever: DensePassageRetriever,
        indexer: BaseIndexer,
        reader: FARMReader,
    ):
        self.index = index
        self.session = session

        self.store = store

        self.retriever = retriever

        self.indexer = indexer
        self.indexer.load()

        self.reader = reader

    def process_question(self, question: str):
        query_embedding = self.retriever.retrieve_embedding(question)

        vector_ids, scores = self.indexer.search(query_embedding, limit=10)
        logger.info(f"\nvector_ids: {vector_ids}\nscores: {scores}")

        docs = self.store.get_documents_by_vector_ids(vector_ids, self.index)

        result = self.reader.predict(question, docs)
        return result

    def get_document_by_id(self, doc_id: int):
        return self.store.get_document_by_id(doc_id)
