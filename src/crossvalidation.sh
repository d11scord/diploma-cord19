python crossvalidation.py \
    --pretrained_model_name_or_path deepset/roberta-base-squad2 \
    --data_dir ../data \
    --train_filename COVID-QA.json \
    --random_seed 42