from typing import List

import numpy as np
from deeppavlov import build_model
from scipy.special import expit


class DeepPavlovReader:
    """
    http://docs.deeppavlov.ai/en/master/features/models/squad.html
    """
    def __init__(
        self,
        config_name: str,
        batch_size: int = 50,
        max_seq_len: int = 256,
        context_window_size: int = 150,
        do_lower_case: bool = False
    ):
        self.config_name = config_name
        self.batch_size = batch_size
        self.max_seq_len = max_seq_len
        self.context_window_size = context_window_size
        self.do_lower_case = do_lower_case

        self.model = build_model(self.config_name, download=True)

    def predict(self, questions: List[str], docs: List[str]):

        predictions = self.model(docs, questions)

        answers = self.get_answers(predictions, docs)
        """
        [
            ['ответ', 'ответ', 'ответ'],          # ответы на вопрос
            [137, 137, 137],                      # начало ответа на вопрос
            [2505075.75, 2505075.75, 2505075.75]  # логиты
        ]
        """

        return answers

    @staticmethod
    def count_prob(score: float):
        return list(expit(np.asarray([score]) / 3))

    @staticmethod
    def create_context_window(ans, offset_answer_start, context_window_size, clear_text):
        """
        https://github.com/deepset-ai/FARM/blob/master/farm/modeling/predictions.py#L118
        """
        len_ans = len(ans)
        context_window_size = max(context_window_size, len_ans + 1)

        len_text = len(clear_text)
        midpoint = int(len_ans / 2) + offset_answer_start
        half_window = int(context_window_size / 2)
        window_start_ch = midpoint - half_window
        window_end_ch = midpoint + half_window

        overhang_start = max(0, -window_start_ch)
        overhang_end = max(0, window_end_ch - len_text)
        window_start_ch -= overhang_end
        window_start_ch = max(0, window_start_ch)
        window_end_ch += overhang_start
        window_end_ch = min(len_text, window_end_ch)
        window_str = clear_text[window_start_ch: window_end_ch]
        return window_str, window_start_ch, window_end_ch

    def get_answers(self, predictions, docs):
        answers = []
        preds = list(zip(*predictions))

        for doc, pred in zip(docs, preds):
            ans, answer_start, logit = pred
            ctx_window, ctx_start, ctx_end = self.create_context_window(
                ans, answer_start, self.context_window_size, doc)
            cur_ans = {
                "answer": ans,
                "probability": self.count_prob(logit)[0],
                "context": ctx_window,
                "offset_start": answer_start - ctx_start,
                "offset_end": answer_start - ctx_start+len(ans),
                "offset_start_in_doc": answer_start,
                "offset_end_in_doc": answer_start+len(ans),

                "doc_left": doc[:answer_start],
                "doc_right": doc[answer_start+len(ans):],
                # "ctx_left": ctx_window[:answer_start - ctx_start],
                # "ctx_right": ctx_window[answer_start - ctx_start+len(ans):]
            }
            answers.append(cur_ans)

        return answers
