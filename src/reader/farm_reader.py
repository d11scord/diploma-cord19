from typing import List

import numpy as np
from farm.infer import QAInferencer
from scipy.special import expit

from src.document_store.schema import Document


class FARMReader:
    """
    https://github.com/deepset-ai/haystack/blob/master/haystack/reader/farm.py
    """
    def __init__(
        self,
        model_name_or_path: str,
        batch_size: int = 50,
        max_seq_len: int = 256,
        doc_stride: int = 128,
        disable_tqdm: bool = False,
        context_window_size: int = 150,
        no_ans_boost: float = 0.0,
        n_best: int = 3,
        use_gpu: bool = False,
        use_fast: bool = True,
    ):
        self.model_name_or_path = model_name_or_path
        self.batch_size = batch_size
        self.max_seq_len = max_seq_len
        self.doc_stride = doc_stride
        self.disable_tqdm = disable_tqdm
        self.context_window_size = context_window_size
        self.no_ans_boost = no_ans_boost
        self.n_best = n_best
        self.use_gpu = use_gpu
        self.use_fast = use_fast

        # "deepset/roberta-base-squad2"
        self.inferencer = QAInferencer.load(
            model_name_or_path, batch_size=batch_size, doc_stride=doc_stride,
            task_type="question_answering", max_seq_len=max_seq_len,
            gpu=use_gpu,
            use_fast=use_fast,
            disable_tqdm=disable_tqdm,
        )

        self.inferencer.model.prediction_heads[0].context_window_size = context_window_size
        self.inferencer.model.prediction_heads[0].no_ans_boost = no_ans_boost
        self.inferencer.model.prediction_heads[0].n_best = n_best + 1

    def predict(self, question: str, docs: List[Document], top_k: int = 10):
        dicts = []
        for doc in docs:
            # QAInput format
            cur_doc = {
                "context": doc.text,
                "qas": [{
                    "question": question,
                    "id": doc.id,
                    "answers": [],
                }]
            }
            dicts.append(cur_doc)

        """
        [{
            'task': 'qa', 
            'predictions': [
                {
                    'question': 'What is the main cause of HIV-1 infection in children', 
                    'id': 10, 
                    'ground_truth': [], 
                    'answers': [
                        {
                            'score': 4.633384704589844, 
                            'probability': None, 
                            'answer': 'no_answer', 
                            'offset_answer_start': 0, 
                            'offset_answer_end': 0, 
                            'context': '', 
                            'offset_context_start': 0, 
                            'offset_context_end': 0, 
                            'document_id': 10
                        }, 
                        {...}
                    ]
                }
            ], 
            'no_ans_gap': -14.314459800720215
        }]
        """
        predictions = self.inferencer.inference_from_dicts(
            dicts=dicts, return_json=False,
            multiprocessing_chunksize=5, streaming=False,
        )

        self.inferencer.close_multiprocessing_pool()

        answers = self.get_answers(predictions, docs, top_k=top_k)

        return answers

    @staticmethod
    def count_prob(score: float, docs_count: int):
        return float(expit(np.asarray(score) / docs_count))

    def get_answers(self, predictions, docs, top_k=None):
        answers = []

        for doc, pred in zip(docs, predictions):
            answers_per_document = []
            for ans in pred.prediction:
                if ans.answer_type == "no_answer":
                    continue
                cur_ans = {
                    "answer": ans.answer,
                    "score": ans.score,
                    "probability": self.count_prob(ans.score, top_k),
                    "context": ans.context_window,
                    "offset_start": ans.offset_answer_start - ans.offset_context_window_start,
                    "offset_end": ans.offset_answer_end - ans.offset_context_window_start,
                    "offset_start_in_doc": ans.offset_answer_start,
                    "offset_end_in_doc": ans.offset_answer_end,
                    "document_id": pred.id,
                    "doc": doc,
                }
                answers_per_document.append(cur_ans)

            answers.extend(answers_per_document[:self.n_best])

        # Сортируем ответы по убыванию их вероятности
        answers = sorted(answers, key=lambda k: k["score"], reverse=True)
        # И выбираем top_k
        answers = answers[:top_k]

        return answers
